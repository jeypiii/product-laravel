<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\Product::create(array(
			'title' => 'test',
			'price' => 1,
			'quantity' => 3,
			'image' => 'pic.png',
			'ingredients' => 'salt',
			'sku' => 'FFF000',
		)); 
    }
}
