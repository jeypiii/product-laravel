import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  
  routes: [
    {
      path: "/",
      alias: ["/products", "/home"],
      name: "products",
      component: () => import("./components/ProductList")
    },
    {
      path: "/products/:id",
      name: "product-details",
      component: () => import("./components/Product")
    },
    {
      path: "/add",
      name: "add",
      component: () => import("./components/AddProduct")
    },
    {
      path: "/chart",
      name: "chart",
      component: () => import("./components/ChartContainer")
    }
  ]
});