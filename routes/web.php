<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//index
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('index');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/products', [App\Http\Controllers\HomeController::class, 'index'])->name('index');


//edit page
Route::get('/products/{var1}', [App\Http\Controllers\HomeController::class, 'index'])->name('edit');

// add page
Route::get('/add', [App\Http\Controllers\HomeController::class, 'index'])->name('add');

// chart page
Route::get('/chart', [App\Http\Controllers\HomeController::class, 'index'])->name('chart');
