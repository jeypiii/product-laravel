<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/products', [App\Http\Controllers\ProductController::class, 'search'])->name('search');
Route::get('/productList', [App\Http\Controllers\ProductController::class, 'getAll'])->name('getAll');

//edit page
Route::get('/products/{var1}', [App\Http\Controllers\ProductController::class, 'show'])->name('product');

Route::put('/products/{var1}', [App\Http\Controllers\ProductController::class, 'update'])->name('update');
