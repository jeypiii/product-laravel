<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Product;

class ProductController extends Controller
{

    public function search(Request $request)
    {
        if (is_numeric($request->page) && is_numeric($request->size)){
            $pageNumber = $request->page + 0;
            $pageSize = $request->size + 0;
        } else {

            $pageNumber = 1;
            $pageSize = null;
            
        }

        $searchTitle = $request->title;

        if(!$this->isNullOrEmptyString($searchTitle))
        {
            return Product::where('title', 'like', '%' . $searchTitle . '%')
            ->orWhere('sku', 'like', '%' . $searchTitle . '%')
            ->Paginate($pageSize, ['*'], 'page', $pageNumber); 
        } else {
            return Product::Paginate($pageSize, ['*'], 'page', $pageNumber); 
        }
    }

    public function getAll()
    {
        return Product::get();
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return $product;
    }

    public function update(Request $request)
    {
        $product = Product::findOrFail($request->_id);
        $product->title = $request->title;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->sku = $request->sku;
        $product->ingredients = $request->ingredients;

        $product->save();

        return $product;
    }



    private function isNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
}
